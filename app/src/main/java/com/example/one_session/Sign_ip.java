package com.example.one_session;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Sign_ip extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_ip);
    }

    public void next (View view) {
        Intent intent = new Intent (this, Sign_up.class);
        startActivity(intent);
    }
}